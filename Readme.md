# Rust Lambda Function with AWS DynamoDB

This project demonstrates how to build a serverless Lambda function using Rust programming language and deploy it to AWS Lambda. The Lambda function interacts with AWS DynamoDB to perform CRUD operations on a table.

## Project Overview

The Rust Lambda function is designed to handle HTTP requests and update a DynamoDB table based on the input data. It uses the AWS Lambda runtime for Rust and the AWS SDK for Rust to interact with DynamoDB. The function is triggered by API Gateway events, enabling it to serve as a backend for web applications or microservices.

### Project Structure

The project structure is as follows:

**my_lambda_project**
1. Cargo.toml
2. src
3. main.rs

- `Cargo.toml`: The Cargo manifest file specifying project dependencies and metadata.
- `src/main.rs`: The main Rust source file containing the Lambda function code.

## Setup Instructions

To set up the project and deploy the Lambda function, follow these steps:

1. **Install Rust**: Ensure that Rust and Cargo are installed on your system. You can install them by following the instructions at [rust-lang.org](https://www.rust-lang.org/tools/install).

2. **Install AWS CLI**: Install the AWS Command Line Interface (CLI) on your system if you haven't already. You can install it by following the instructions at [AWS CLI Installation](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html).

3. **Configure AWS Credentials**: Configure your AWS credentials on your system using the AWS CLI. You can do this by running `aws configure` and providing your AWS Access Key ID, Secret Access Key, and default region.

4. **Create DynamoDB Table**: Create a DynamoDB table using the AWS Management Console or AWS CLI. You can define the table schema according to your requirements. For example:

## Clone the Repository:
`git clone https://github.com/your-username/my-lambda-project.git`

`cd my-lambda-project`

## Build the Project: 
`cargo lambda build`

## Deploy the Lambda Function:
`cargo lambda deploy --iam-role <IAM_ROLE_ARN>`


**Test the Lambda Function:** 
Test the deployed Lambda function by invoking it using API Gateway, AWS SDK, or AWS CLI.

**Input Payload Format**
The input payload format for the Lambda function is as follows:

{
  "id": "string",
  "name": "string",
  "gender": "string"
}

## Dependencies:
The project uses the following dependencies:
1. lambda_http = "0.10.0"
2. lambda_runtime = "0.9.2"
3. serde = "1.0.136"
4. serde_json = "1.0.78"
5. aws-config = "0.51.0"
6. aws-sdk-dynamodb = "0.21.0"
7. rand = "0.8.4"
8. simple_logger = "1.16.0"
9. tracing = { version = "0.1", features = ["log"] }
10. tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }
11. tokio = { version = "1", features = ["macros"] }

![alt text](images/deploy.png)
![alt text](images/test.png)
![alt text](images/DynamoDb_Update.png)


